"""Test for my_class.py
"""

from my_package.my_class import my_class

my_obj = my_class(3)


def test_print_num():
    assert my_obj.print_num() == "3"


def test_add_num():
    assert my_obj.add_num(2) == 5

if __name__ == "__main__":
    test_print_num()
    test_add_num()
