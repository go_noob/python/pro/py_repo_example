"""My class
"""


class my_class:
    def __init__(self, num: int):
        print("Initialize of my_class")
        self._num: int = num

    def print_num(self) -> str:
        print(self._num)
        return str(self._num)

    def add_num(self, num: int) -> int:
        return self._num + num
